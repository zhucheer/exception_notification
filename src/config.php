<?php
/*
 * Log记录器提供了 7 种在 RFC 5424 标准内定义的记录等级:
 * debug, info, notice, warning, error, critical, and alert.
 * 
 */

return [
	//邮件通知类型，1:普通邮件发送(不建议此方式，会造成报错时页面加载速度会变慢) 2：队列方式，请按照laravel的队列要求部署好相关环境
	'notification_mail_type'=>	1,
	
	//通知邮件标的
	'notification_mail_title'=>	'来自系统的报错通知',

	//指定发送通知的错误级别
	'notification_level'=>	['warning','error','critical','alert'],
	
	//通知报错的收件人
	'notification_emails' => ['admin@local.com'],
	
	//通知频率，相同的错误信息，多长时间通知一次，避免报警瀑布,单位（分钟）,
	'notification_frequency'=>5,
];