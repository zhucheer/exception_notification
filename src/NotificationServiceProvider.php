<?php namespace Cheer\ExceptionNotification;
/**
 * @author zhucheer
 * @license MIT
 * @version v1.0.0
 */

use Illuminate\Support\ServiceProvider;
use Log;
use Cache;
use Mail;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	$config_path = __DIR__.DIRECTORY_SEPARATOR.'config.php';
    	$this->publishes([
    			$config_path => config_path('ExceptionNotification.php'),
    	]);
    	$this->loadViewsFrom(__DIR__, 'notificationView');
    	
     	
    	
    	//侦听日志
    	Log::listen(function($e) {
            $level = $e->level;
            $message = $e->message;
            $context = $e->context;
    		//需要邮件通知的级别
    		$notificationLevel = config('ExceptionNotification.notification_level');
    		$noticeMsg['app_env'] = env('APP_ENV');
    		$noticeMsg['level'] = $level;
    		$noticeMsg['url'] = request()->url();
    		$noticeMsg['query'] = request()->getRequestUri();
    		if($message instanceof \Exception){
	    		$noticeMsg['notice'] = $message->getMessage();
	    		$noticeMsg['file'] = $message->getFile();
	    		$noticeMsg['line'] = $message->getLine();
	    		$noticeMsg['trace'] = $message->getTraceAsString();
    		}else{
    			$noticeMsg['notice'] = $message;
    		}
    	
    		if(in_array($level, $notificationLevel)){
    			$this->mailNotification($noticeMsg);
    		}
    	});
    	
    	
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    	$config_path = __DIR__.DIRECTORY_SEPARATOR.'config.php';
    	$this->mergeConfigFrom(
    			$config_path, 'ExceptionNotification'
    	);
    }
    
    
    /**
     * 邮件通知处理
     * @param $noticeMsg 通知邮件模版数据
     */
    private  function mailNotification($noticeMsg){
    	$notification_frequency = config('ExceptionNotification.notification_frequency');
    	$notificationHashKey = 'SysException_'.md5($noticeMsg['app_env'].$noticeMsg['notice']);//报错哈希，相同的报错5分钟报一次
    	$hasKey = Cache::has($notificationHashKey);
    	if($hasKey){
    		return false;//如果存在了就不做操作了
    	}
    	Cache::put($notificationHashKey, 1, $notification_frequency);
    	
    	$view = 'notificationView::notification';
    	$notification_emails = config('ExceptionNotification.notification_emails');
    	$notification_type = config('ExceptionNotification.notification_mail_type');
    	$mailTitle = config('ExceptionNotification.notification_mail_title');
    	
    	if(!empty($notification_emails) && view()->exists($view)){
    		if(1 == $notification_type){
    			Mail::send($view, $noticeMsg, function($message)use($notification_emails, $mailTitle)
    			{
			    	foreach($notification_emails as $item){
			    		$message->to($item, $item);
			    	}
			    	$message->subject($mailTitle);
    			});
    		}
    		if(2 == $notification_type){
    			Mail::queue($view, $noticeMsg, function($message)use($notification_emails, $mailTitle)
    			{
    				$mailTitle = config('ExceptionNotification.notification_mail_title');
			    	foreach($notification_emails as $item){
			    		$message->to($item, $item);
			    	}
			    	$message->subject($mailTitle);
    			});
    		}
    	}   	
    }
    
    
}
