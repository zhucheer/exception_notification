#侦听Laravel的Log日志,将指定级别的报错发送通知邮件
**使用前**
- 该包适用于Laravel5
- 使用前请确保已经配置好app/config/mail.php中的相关内容，并能正常使用发送邮件功能
- 该包提供队列邮件发送，在使用队列发送功能前，确保已经配置好Laravel队列相关功能，具体实现可以查看[Laravel队列](http://www.golaravel.com/laravel/docs/5.0/queues/)


**安装**
```
composer require cheer/exception_notification
```

**使用**
首先执行下发布将包中的配置文件复制到项目中
```
php artisan vendor:publish
```
将会生成该配置文件，打开编辑app/config/ExceptionNotification.php
将notification_emails项配置成一个正确的收件人Email，其他配置想可按照自己的需求调整

然后编辑app/config/app.php
在app.php的providers节点下增加以下代码
```
Cheer\ExceptionNotification\NotificationServiceProvider::class
```

至此，每当发生指定级别的报错Log时，将会发送邮件通知


